package com.bruma.dispatcher

import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.location.LocationListener
import android.widget.Toast
import java.util.*
import android.R.string.cancel
import android.content.DialogInterface
import com.bruma.dispatcher.R.styleable.AlertDialog


class MyLocationListener(manager: LocationManager, userId: Int, context:Context) : LocationListener {
    var tvStatusGPS: String? = null
    var tvStatusNet: String? = null
    var locationManager = manager
    var Context = context
    var UserId = userId

        override fun onLocationChanged(location: Location) {
            showLocation(location)
        }

        override fun onProviderDisabled(provider: String) {
            if(provider == LocationManager.GPS_PROVIDER) {
                val builder = android.support.v7.app.AlertDialog.Builder(Context)
                builder.setTitle("Ошибка подключения")
                    .setMessage("Нет подключения по GPS. Включите геолокацию в настройках.")
                    .setCancelable(false)
                    .setNegativeButton("ОК",
                        DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
                val alert = builder.create()
                alert.show()
            }
        }

        override fun onProviderEnabled(provider: String) {
            showLocation(locationManager.getLastKnownLocation(provider))
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            if (provider == LocationManager.GPS_PROVIDER) {
                tvStatusGPS = "Status: " + status.toString()
            } else if (provider == LocationManager.NETWORK_PROVIDER) {
                tvStatusNet = "Status: " + status.toString()
            }
        }

    private fun showLocation(location: Location?) {
        if (location == null)
            return
        DispatcherApi().UpdateUserLocation(id = UserId, location =
        com.bruma.dispatcher.Location(location!!.getLatitude(),location!!.getLongitude()))
    }
}
