package com.bruma.dispatcher

class RequestsItem(id:Int, title:String, info:String, date:String, address:String, photos:String, status:String) {
    var  Title = title
    var  Description = info
    var  Address = address
    var  Date = date
    var  Photos = photos
    var  Status = status
    var  Id = id
}