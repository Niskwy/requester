package com.bruma.dispatcher

import org.json.JSONObject

class DispatcherApi : IApi {

    override fun GetNewRequests(userId: Int, url: String): JSONObject {
        val get = Get()
        return JSONObject(get.execute(url).get())
    }

    override fun GetCurrentRequests(userId: Int, url: String): JSONObject {
        val get = Get()
        return JSONObject(get.execute(url).get())
    }

    override fun GetFinishedRequests(userId: Int, url: String): JSONObject {
        val get = Get()
        return JSONObject(get.execute(url).get())
    }

    override fun GetUserByLogin(url: String, login: String, pass: String): JSONObject {
        val post = Post()
       // var obj = JSONObject()
        val str="login=$login&password=$pass"
        //obj.put("login",login)
        //obj.put("password",pass)
        return JSONObject(post.execute(url,str).get())
    }

    override fun GetUserById(url: String, id: Int) : JSONObject{
        val post = Post()
        val str ="id=$id"
        return JSONObject(post.execute(url,str).get())
    }

    override fun UpdateRequest(url: String, id: Int, status: String): JSONObject {
        val post = Post()
        val str = "id=$id&status=$status"
        return JSONObject(post.execute(url,str).get())
    }

    override fun UpdateUserData(url: String, id: Int, name: String, login: String, password: String): JSONObject {
        val post = Post()
        val params = "id=$id&name=$name&login=$login&password=$password"
        return  JSONObject(post.execute(url, params).get())
    }

    override fun InsertNewUser(url: String, name: String, login: String, password: String): JSONObject {
        val post = Post()
        val params = "name=$name&login=$login&password=$password"
        return JSONObject(post.execute(url, params).get())
    }

    override fun UpdateUserLocation(url: String, id: Int, location: Location): JSONObject {
        val post = Post()
        val params ="id=$id&latitude=${location.latitude}&longitude=${location.longitude}"
        return JSONObject(post.execute(url, params).get())
    }
}