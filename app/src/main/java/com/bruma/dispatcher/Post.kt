package com.bruma.dispatcher

import android.app.DownloadManager
import android.os.AsyncTask
import java.io.*
import java.net.HttpURLConnection
import java.net.URL



class Post  : AsyncTask<String, Void, String>(){

    @Throws(IOException::class)
    override fun doInBackground(vararg params: String): String
    {
        var data = ""
        val url = URL(params.component1())
        val httpClient = url.openConnection() as HttpURLConnection
        httpClient.requestMethod  = "POST"
        httpClient.doOutput = true
        httpClient.doInput = true
        httpClient.setRequestProperty("Content-Length", "" + Integer.toString(params.component2().toByteArray(
            charset("UTF-8")).size))
        val out = httpClient.outputStream
        out.write(params.component2().toByteArray(charset("UTF-8")))
        out.close()
        httpClient.connect()
        if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
            try {
                var line: String
                val br = BufferedReader(InputStreamReader(httpClient.inputStream))
                line = br.readLine()
                while (line != null) {
                    data += line
                    line = br.readLine()
                }
                return data
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }
        } else {
            println("ERROR ${httpClient.responseCode}")
        }
        return data
    }


    fun readStream(inputStream: BufferedInputStream): String {
        val bufferedReader = BufferedReader(InputStreamReader(inputStream))
        val stringBuilder = StringBuilder()
        bufferedReader.forEachLine { stringBuilder.append(it) }
        return stringBuilder.toString()
    }
}