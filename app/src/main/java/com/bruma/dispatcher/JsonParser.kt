package com.bruma.dispatcher

import android.service.autofill.UserData
import org.json.JSONException
import org.json.JSONObject

class JsonParser {
    fun ParseRequest(jsonObj : JSONObject): List<RequestsItem>{
        try {
            val requests = jsonObj.getJSONArray("requests")
            var requestList = mutableListOf<RequestsItem>()
            for (i in 0..requests!!.length()-1) {
                val request = requests.getJSONObject(i)
                var requestItem = RequestsItem(request.getInt("id"),
                    request.getString("title"),
                    request.getString("description"),
                    request.getString("date"),
                    request.getString("address"),
                    "Фотографии",
                    request.getString("status"))
                requestList.add(requestItem)
            }
            return requestList
        }
        catch (e: JSONException) {
            throw JSONException("Error parsing of JSON")
        }
        return listOf()
    }

    fun ParseUser(jsonObj : JSONObject): User?{
        try {
            //val user = jsonObj.getJSONObject("user")
            var userData = User(
                jsonObj.getInt("id"),
                jsonObj.getString("name"),
                jsonObj.getString("login"),
                jsonObj.getString("password"),
                jsonObj.getString("state")
            )
            return userData
        }
        catch (e: JSONException) {
            println("Error parsing of JSON")
        }
        return null
    }
}