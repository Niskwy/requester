package com.bruma.dispatcher.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bruma.dispatcher.DispatcherApi
import com.bruma.dispatcher.JsonParser
import com.bruma.dispatcher.R
import kotlinx.android.synthetic.main.page_profile.*
import kotlinx.android.synthetic.main.page_request_details.*

class ProfileActivity : AppCompatActivity() {
    var UserProfileId =-1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_profile)
        UserProfileId = intent.getIntExtra("Id", -1)
        LoadProfileData()
        saveButton.setOnClickListener(this::OnSaveClick)
        retr_btn.setOnClickListener(this::OnReturnClick)
    }

    private fun LoadProfileData(){
        val obj = DispatcherApi().GetUserById(id=UserProfileId)
        val user = JsonParser().ParseUser(obj)
        if(user!=null){
            name_profile.setText(user.Name)
            mail_profile.setText(user.Login)
            pass_profile.setText(user.Password)
        }
        else {
            Toast.makeText(this, "Ошибка получения данных пользователя", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    private fun OnSaveClick(view: View){
        DispatcherApi().UpdateUserData(id = UserProfileId, name = name_profile.text.toString(),
            login = mail_profile.text.toString(), password = pass_profile.text.toString())
        Toast.makeText(this, "Данные успешно изменены", Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun OnReturnClick(view: View){
        finish()
    }
}
