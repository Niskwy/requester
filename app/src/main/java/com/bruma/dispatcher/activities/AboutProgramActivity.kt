package com.bruma.dispatcher.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bruma.dispatcher.R
import kotlinx.android.synthetic.main.page_request_details.*

class AboutProgramActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_about_program)
        rtrn_btn.setOnClickListener(this::OnReturnClick)
    }

    private fun OnReturnClick(view: View){
        finish()
    }
}
