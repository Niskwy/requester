package com.bruma.dispatcher.activities

import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.bruma.dispatcher.DispatcherApi
import com.bruma.dispatcher.R
import kotlinx.android.synthetic.main.page_request_details.*



class RequestDetailsActivity() : AppCompatActivity() {
    private var Id = 0
    private var  Title = "Title"
    private var  Description = "Info"
    private var  Address = "Address"
    private var  Date = "Date"
    private var  Status = "Status"
    val Statuses = arrayOf("Одобрено","Принято","Завершено")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_request_details)
        intent = getIntent()
        RequestName.text = intent.getStringExtra("Title")
        Info.text = intent.getStringExtra("Info")
        address.text = intent.getStringExtra("Address")
        date.text = intent.getStringExtra("Date")
        //photo.text = "Фотографии"
        status.text = intent.getStringExtra("Status")
        if(status.text == Statuses[2])
            ready_btn.visibility = View.INVISIBLE
        else if(status.text == Statuses[0])
            ready_btn.text = Statuses[1]
        Id = intent.getIntExtra("Id",0)
        rtrn_btn.setOnClickListener(this::OnReturnClick)
        ready_btn.setOnClickListener(this::OnReadyClick)
    }

    private fun OnReturnClick(view: View){
        finish()
    }

    private fun OnReadyClick(view: View){
        var state : String
        var message : String
        if(status.text == Statuses[0]) {
            state = Statuses[1]
            message = "Заявка перемещена в текущие"
        }
        else {
            state = Statuses[2]
            message = "Заявка перемещена в завершенные"
        }
        val result = DispatcherApi().UpdateRequest(id = Id,status = state)
        if(result.getInt("success")==1) {
            if(state == Statuses[2]) {
                status.text = state
                ready_btn.visibility = View.INVISIBLE
            }
            else if(state == Statuses[1]) {
                status.text = state
                ready_btn.text = "Выполнено"
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
        else{
            Toast.makeText(this, "Ошибка. "+result.getString("message"), Toast.LENGTH_SHORT).show()
        }
    }
}