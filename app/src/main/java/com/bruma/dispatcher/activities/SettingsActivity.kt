package com.bruma.dispatcher.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.bruma.dispatcher.R
import kotlinx.android.synthetic.main.page_request_details.*
import kotlinx.android.synthetic.main.page_settings.*

class SettingsActivity : AppCompatActivity() {
    var UserId = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_settings)
        UserId = intent.getIntExtra("UserId", -1)
        set_rtrn_btn.setOnClickListener(this::OnReturnClick)
        val adapter = ArrayAdapter<String>(this, R.layout.cell_list, arrayOf("Изменить данные профиля"))
        settingsList.setAdapter(adapter)
        settingsList.setOnItemClickListener { parent, view, position, id ->
            OnItemClick(parent, view, position, id)
        }
    }

    private fun OnReturnClick(view: View){
        finish()
    }

    private fun OnItemClick(parent: AdapterView<*>, view: View, pos: Int, id: Long){
        if(pos==0) {
            val intent = Intent(this, ProfileActivity::class.java)
            intent.putExtra("Id", UserId)
            startActivity(intent)
        }
    }
}
