package com.bruma.dispatcher.activities

import android.content.Intent

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button;
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.page_entry.*
import org.json.JSONObject
import org.json.JSONException
import java.lang.Exception
import java.lang.NullPointerException
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import com.bruma.dispatcher.ConnectionChecker
import com.bruma.dispatcher.DispatcherApi
import com.bruma.dispatcher.JsonParser
import com.bruma.dispatcher.R

class MainActivity : AppCompatActivity() {
    private lateinit var Login: EditText
    private lateinit var Password: EditText
    private lateinit var SignIn: Button
    private lateinit var SignUp: Button
    private lateinit var Hint: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_entry)
        SetElements()
        SignIn.setOnClickListener(this::OnSignInClick)
        SignUp.setOnClickListener(this::OnSignUpcClick)

    }

    private fun SetElements() {
        Login = loginEditText
        Password = passEditText
        SignIn = signInButton
        SignUp = signUpButton
        Hint = hint
        Hint.visibility = View.INVISIBLE
    }

    private fun OnSignInClick(view: View) {
        try {
            if(ConnectionChecker().isOnline(this)) {
                val result =
                    DispatcherApi().GetUserByLogin(login = Login.text.toString(), pass = Password.text.toString())
                if (result.getInt("success") == 1) {
                    intent = Intent(this, RequestsActivity::class.java)
                    intent.putExtra("UserId", result.getInt("id"))
                    startActivity(intent)
                } else {
                    Hint.text = "Неверно введен логин и/или пароль"
                    Hint.visibility = View.VISIBLE
                }
            }
            else {
                val builder = AlertDialog.Builder(this@MainActivity)
                builder.setTitle("Ошибка")
                    .setMessage("Ошибка подключения к сети. Проверьте соединение с Интернетом.")
                    .setCancelable(false)
                    .setNegativeButton("ОК",
                        DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
                val alert = builder.create()
                alert.show()
            }
        }
        catch(exp: NullPointerException) {
            Hint.text = "Введите логин и пароль"
            Hint.visibility = View.VISIBLE
        }
        catch(exp: Exception) {
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle("Ошибка")
                .setMessage("Сервер недоступен")
                .setCancelable(false)
                .setNegativeButton("ОК",
                    DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
            val alert = builder.create()
            alert.show()
        }
        //startActivity(Intent(this, SettingsActivity:: class.java))
    }

    private fun OnSignUpcClick(view: View) {
        startActivity(Intent(this, RegistrationActivity:: class.java))
    }
}
