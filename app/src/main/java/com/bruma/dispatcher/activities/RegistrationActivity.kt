package com.bruma.dispatcher.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bruma.dispatcher.DispatcherApi
import com.bruma.dispatcher.R
import kotlinx.android.synthetic.main.page_registration.*
import org.mindrot.jbcrypt.BCrypt

class RegistrationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_registration)
        signUpButton.setOnClickListener(this::OnSignUpClick)
    }

    private fun OnSignUpClick(view: View){
        var pass1 = BCrypt.hashpw(pass.text.toString(), BCrypt.gensalt())
        if(pass.text.toString().equals(repeat_pass.text.toString())) {
            DispatcherApi().InsertNewUser(
                name = firstname.text.toString(), login = mail.text.toString(),
                password = pass.text.toString()
            )
            Toast.makeText(this, "Ваш профиль зарегистрирован", Toast.LENGTH_SHORT).show()
            finish()
        }
        else {
            hint.text = "Пароли не совпадают"
            pass.setText("")
            repeat_pass.setText("")
        }
    }

    private fun OnReturClick(view: View){
        finish()
    }
}
