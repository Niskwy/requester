package com.bruma.dispatcher.activities

import android.content.Intent
import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.bruma.dispatcher.*
import kotlinx.android.synthetic.main.page_current.*

class CurrentActivity : Fragment(){
    private var mPage = 0
    var UserId = -1
    lateinit var requests: MutableMap<String, RequestsItem>

    companion object {
        const val ARG_PAGE = "ARG_PAGE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var args = arguments
        if (args != null)
            mPage = args.getInt(ARG_PAGE)
        UserId = activity!!.intent.getIntExtra("UserId",-1)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.page_current, container, false)
        return view
    }

    override fun onStart() {
        super.onStart()
        val list = JsonParser().ParseRequest(DispatcherApi().GetCurrentRequests(userId  = UserId))
        requests = mutableMapOf()
        for (i in 0..list.count()-1)
            requests.put(list.elementAt(i).Title, list.elementAt(i))
        val adapter = ArrayAdapter<String>(this.context, R.layout.cell_list, requests.keys.toTypedArray())
        currentList.setAdapter(adapter)
        currentList.setOnItemClickListener { parent, view, position, id ->
            OnItemClick(parent, view, position, id)
        }
    }

    private fun OnItemClick(parent: AdapterView<*>, view: View,pos: Int,id: Long){
        var intent = Intent(this.context, RequestDetailsActivity::class.java)
        intent.putExtra("Id",requests.values.elementAt(pos).Id)
        intent.putExtra("Title",requests.values.elementAt(pos).Title)
        intent.putExtra("Info",requests.values.elementAt(pos).Description)
        intent.putExtra("Date",requests.values.elementAt(pos).Date)
        intent.putExtra("Address",requests.values.elementAt(pos).Address)
        intent.putExtra("Status",requests.values.elementAt(pos).Status)
        startActivity(intent)
    }
}

