package com.bruma.dispatcher.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.bruma.dispatcher.*
import kotlinx.android.synthetic.main.page_requests.*
import kotlinx.android.synthetic.main.cell_action_bar.*
import kotlinx.android.synthetic.main.cell_requests.*


class RequestsActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var UserId = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        UserId = intent.getIntExtra("UserId",-1)
        setContentView(R.layout.page_requests)
        SetPager()
        SetActionBar()
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
    }

    private  fun SetPager(){
        var tabTitles  = mapOf("Текущие" to CurrentActivity(),"Новые" to NewActivity(), "Завершенные" to FinishedActivity())
        Pager.adapter = FragmentPagerAdapter(supportFragmentManager, this, tabTitles)
        tabs.setupWithViewPager(Pager)
    }

    private fun SetActionBar() {
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_requests -> {
                drawer_layout.closeDrawers()
            }
            R.id.nav_settings -> {
                var intent = Intent(this, SettingsActivity:: class.java)
                intent.putExtra("UserId", UserId)
                startActivity(intent)
            }
            R.id.nav_about -> {
                startActivity(Intent(this, AboutProgramActivity:: class.java))
            }
            R.id.nav_exit -> {
                startActivity(Intent(this, MainActivity:: class.java))
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    var locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
                    var locationListener = MyLocationListener(locationManager, UserId, this)
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        1000*10, 0f, locationListener);
                    locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 1000*10, 0f,
                        locationListener)
                } else {
                    Toast.makeText(this,
                        "Вы отказались от доступа к вашей геолокации. Включить ее Вы можете в настройках.",
                        Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }
}
