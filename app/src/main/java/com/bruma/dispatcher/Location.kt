package com.bruma.dispatcher

data class Location(val latitude : Double, val longitude : Double) {
}