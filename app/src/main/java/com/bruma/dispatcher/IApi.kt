package com.bruma.dispatcher

import org.json.JSONObject
import java.net.URL

interface IApi {
    fun GetNewRequests(userId: Int, url: String =
        "http://buddy.webud.ru/api/requests/get_requests_by_user_id.php?id=$userId&&status=1") : JSONObject
    fun GetCurrentRequests(userId : Int,url : String =
        "http://buddy.webud.ru/api/requests/get_requests_by_user_id.php?id=$userId&&status=2"): JSONObject
    fun GetFinishedRequests(userId :Int, url : String =
        "http://buddy.webud.ru/api/requests/get_requests_by_user_id.php?id=$userId&&status=4"): JSONObject
    fun GetUserByLogin(url : String = "http://buddy.webud.ru/api/users/get_user_by_login.php", login : String,
                       pass : String): JSONObject
    fun GetUserById(url : String = "http://buddy.webud.ru/api/users/get_user_by_id.php", id : Int) : JSONObject
    fun UpdateRequest(url : String = "http://buddy.webud.ru/api/requests/update_request.php", id : Int, status : String) : JSONObject
    fun UpdateUserData(url : String = "http://buddy.webud.ru/api/users/update_user_by_id.php", id : Int, name : String,
                       login : String, password : String) : JSONObject
    fun InsertNewUser(url : String = "http://buddy.webud.ru/api/users/insert_new_user.php", name : String,
                      login : String, password : String) : JSONObject
    fun UpdateUserLocation(url : String = "http://buddy.webud.ru/api/users/update_geodata_by_userid.php", id : Int, location : Location) : JSONObject
}