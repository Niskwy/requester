package com.bruma.dispatcher

import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.content.Context
import android.os.Bundle

class FragmentPagerAdapter(fm: FragmentManager, private val context: Context,
                           private val tabs : Map<String,Fragment>)
    : FragmentPagerAdapter(fm) {
    private val tabsTitles : Map<String,Fragment>
    init {
        tabsTitles = tabs
    }

    override fun getCount(): Int {
        return tabsTitles.count()
    }

    override fun getItem(position: Int): Fragment {
        val args = Bundle()
        args.putInt("ARG_PAGE", position+1)
        var fragment = tabs.values.elementAt(position)
        fragment.setArguments(args)
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabsTitles.keys.elementAt(position)
    }
}