package com.bruma.dispatcher

class User(id: Int, name: String, login: String, password: String, state: String) {
    val Id = id
    val Name = name
    val Login = login
    val Password = password
    val State = state
}